<?php

class Trip
{
  public $start = "";
  public $end ="";
  public $car = "";
  public $start_date="";
  public $end_date="";
  public $trip_id=0;
  
  public function setStart($start) {
      	$this->start=$start;
  	}	
  public function getStart(){
      return $this->start;  	
  	}

  public function setEnd($end) {
      	$this->end=$end;
  	}	
  public function getEnd(){
      return $this->end;  	
  	}
  	
  public function setCar($car) {
      	$this->car=$car;
  	}	
  public function getCar(){
      return $this->car;  	
  	}

  public function setStartDate($start_date) {
      	$this->start_date=$start_date;
  	}	
  public function getStartDate(){
      return $this->start_date;  	
  	}

  public function setEndDate($end_date) {
      	$this->end_date=$end_date;
  	}	
  public function getEndDate(){
      return $this->end_date;  	
  	}
  	
  public function setTripId($trip_id) {
      	$this->trip_id=$trip_id;
  	}	
  public function getTripId(){
      return $this->trip_id;  	
  	}

	
}


?>