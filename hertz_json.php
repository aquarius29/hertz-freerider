<?php 
header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");

ini_set('display_errors', 'On');
error_reporting(E_ALL);


include("simple_html_dom.php");
include("trips.php");
include("logic.php");

$url = "http://www.hertzfreerider.se/unauth/list_transport_offer.aspx"; 

$html = file_get_html($url);


$trip_coll = array();

$trip_coll = parse_site($html);
echo print_json($trip_coll);



function print_json($trip_coll) {
$trips = array();
$i = 0;
$output = "" ;
$output2 = ""; 
//$color = 1; 

//usort($trip_coll,'cmp');

foreach ($trip_coll as $trip){
	$num = $trip->getTripId();
	if($i<10){
		$num = "0" . $i ;
	}else{
		$num = $i ;
	}
$trip_array = array();

$output .= 'trip:{';

$output .= 'start:"' . $trip ->getStart()  .  '",'; 
$output .= 'end:"' . $trip->getEnd()  .  '",'; 

//$output .= "<a href='https://maps.google.com/maps?saddr=" . $trip->getStart() . "&daddr=" . $trip->getEnd() . "' target='_blank' id='from'><img src='img/route.png' width='50px'/>Show Route</a>\n";

$output .= 'start_date:"' . $trip -> getStartDate() . '",'; 

$output .= 'end_date:"' .$trip -> getEndDate() . '",'; 
$output .= 'car:"' . $trip -> getCar() . '"'; 
//$output.= "<a id='ctl00_ContentPlaceHolder1_Display_transport_offer_advanced1_DataList1_ctl".$num."_Button1' class='book' href=\"javascript:__doPostBack('ctl00\$ContentPlaceHolder1\$Display_transport_offer_advanced1\$DataList1\$ctl".$num."\$Button1','')\">Book this ride</a>\n";

$output .= "},";

$output2 = $output . $output2 ."\n";
//if($color == 1){$color =2;}else{$color =1;}
$trip_array = array("start"=>$trip ->getStart(), "end"=>$trip ->getEnd(), "start_date"=> $trip->getStartDate(),"end_date"=>$trip->getEndDate(), "car"=> $trip -> getCar());
$array_id = "trip".$i;
$trips[] = $trip_array;

$i++;
}

print json_encode(array('trips'=>$trips));

}
?>
